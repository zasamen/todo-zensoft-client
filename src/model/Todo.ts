export default interface Todo {
  id: number;
  name: string;
  description: string;
  isDone: boolean;
}
