import React, { useState, useEffect } from 'react';
import './App.css';
import TodoList from './components/TodoList';
import TodoEditor from './components/TodoEditor';
import Todo from './model/Todo';
import fetch from 'isomorphic-fetch';

const url =
  process.env.REACT_APP_SERVER_URL || '/api/todo';

const handleResponseErrors:
  | ((value: Response) => void | PromiseLike<void>)
  | null
  | undefined = response => {
  if (!response.ok)
    response.json().then(({ error }) => {
      throw new Error(error);
    });
};

const handleRequestErrors = (error: { message: string } | undefined) => {
  if (error) alert(error.message);
};

const addOrUpdateTodo = async (todo: Todo): Promise<void> => {
  let newUrl = url;
  const headers = {
    'Content-Type': 'application/json'
  };
  let method,
    newTodo = todo;
  if (todo.id === -1) {
    method = 'POST';
    const id = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    newTodo = { ...todo, id };
  } else {
    newUrl += '/' + newTodo.id;
    method = 'PUT';
  }
  const body = JSON.stringify(newTodo);
  return await fetch(newUrl, { method, headers, body })
    .then(handleResponseErrors)
    .catch(handleRequestErrors);
};

const getTodos = async () => {
  const method = 'GET';
  const headers = {
    'Content-Type': 'application/json'
  };
  return fetch(url, { method, headers }).then(response => response.json());
};

const getTodo = async (id: number) => {
  const method = 'GET';
  const headers = {
    'Content-Type': 'application/json'
  };
  return fetch(url + '/' + id, { method, headers }).then(response =>
    response.json()
  );
};

const deleteTodo = async ({ id }: { id: number }) => {
  const method = 'DELETE';
  const headers = {
    'Content-Type': 'application/json'
  };
  return await fetch(url + '/' + id, { method, headers })
    .then(handleResponseErrors)
    .catch(handleRequestErrors);
};

const App: React.FC = () => {
  const [todo, setTodo] = useState<Todo | undefined>();
  const [todoId, setTodoId] = useState<number | null>(null);
  const [todos, setTodos] = useState<Todo[] | undefined>();
  useEffect(() => {
    getTodos().then(setTodos);
  }, [todo]);
  useEffect(() => {
    if (todoId) getTodo(todoId).then(setTodo);
  }, [todoId]);
  return (
    <div className="App">
      <TodoEditor
        todo={todo}
        addOrUpdateTodo={addOrUpdateTodo}
        deleteTodo={deleteTodo}
      />
      <TodoList todos={todos} selectTodo={({ id }) => setTodoId(id)} />
    </div>
  );
};

export default App;
