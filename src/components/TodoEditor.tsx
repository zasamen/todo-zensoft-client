import React, { FC, useState, ChangeEvent, useEffect, Fragment } from 'react';
import './TodoEditor.css';
import Todo from '../model/Todo';

interface TodoEditorProps {
  todo?: Todo;
  addOrUpdateTodo?: (todo: Todo) => void | Promise<void>;
  deleteTodo?: ({ id }: { id: number }) => void | Promise<void>;
}

const defaultTodo: Todo = { id: -1, name: '', description: '', isDone: false };

const useTodo = (
  initial: Todo | undefined,
  setTodo: React.Dispatch<React.SetStateAction<Todo>>
) =>
  useEffect(() => {
    if (initial) setTodo(initial);
  }, [initial, setTodo]);

const onChange = <T extends {}>(t: T, setT: (t: T) => void) => ({
  target
}: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
  const { type, name, value } = target;
  setT({
    ...t,
    [name]: type === 'checkbox' ? (target as HTMLInputElement).checked : value
  });
};

const onClick = <T extends {}>(f: undefined | ((t: T) => void), t: T) => () =>
  f && f(t);

const TodoEditor: FC<TodoEditorProps> = ({
  todo: initial,
  addOrUpdateTodo,
  deleteTodo
}: TodoEditorProps) => {
  const [todo, setTodo] = useState<Todo>(defaultTodo);
  useTodo(initial, setTodo);
  const onTodoChange = onChange(todo, setTodo);
  return (
    <div className="todo-editor">
      <LabeledInput
        name="id"
        label="ID"
        disabled={true}
        type="number"
        value={todo.id}
      />
      <LabeledInput
        name="name"
        label="Name"
        value={todo.name}
        onChange={onTodoChange}
      />
      <LabeledArea
        name="description"
        label="Description"
        onChange={onTodoChange}
        value={todo.description}
      />
      <LabeledInput
        name="isDone"
        label="Done? "
        type="checkbox"
        checked={todo.isDone}
        onChange={onTodoChange}
      />
      <button onClick={onClick(addOrUpdateTodo, todo)}>OK</button>
      <button onClick={onClick(deleteTodo, todo)}>RM</button>
      <button onClick={onClick(setTodo, defaultTodo)}>CLR</button>
    </div>
  );
};

interface LabeledInputProps {
  name: string;
  label: string;
  value?: number | string | string[] | undefined;
  type?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
  disabled?: boolean;
  checked?: boolean;
}

const LabeledInput: FC<LabeledInputProps> = ({
  name,
  label,
  value,
  onChange = () => {},
  type = 'text',
  disabled = false,
  checked = false
}: LabeledInputProps) => {
  return (
    <Fragment>
      <label htmlFor={name}>{label} </label>
      <input
        type={type}
        disabled={disabled}
        name={name}
        value={value}
        onChange={onChange}
        checked={checked}
      />
    </Fragment>
  );
};

const LabeledArea: FC<LabeledInputProps> = ({
  name,
  label,
  value,
  onChange = () => {}
}: LabeledInputProps) => {
  return (
    <Fragment>
      <label htmlFor={name}>{label} </label>
      <textarea name={name} value={value} onChange={onChange} />
    </Fragment>
  );
};

export default TodoEditor;
