import React, { FC } from 'react';
import Todo from '../model/Todo';

interface TodoItemProps {
  todo: Todo;
  getTodo: ({ id }: { id: number }) => void;
}

const TodoItem: FC<TodoItemProps> = ({ todo, getTodo }: TodoItemProps) => {
  const { name, isDone } = todo;
  const style = {
    textDecoration: isDone ? 'line-through' : '',
    listStyleType: 'none',
    paddingTop: '1em',
    marginLeft: 'auto',
    marginRight: 'auto'
  };
  return (
    <li onClick={() => getTodo(todo)} style={style}>
      {name}
    </li>
  );
};

export default TodoItem;
