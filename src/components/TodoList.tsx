import React, { FC } from 'react';
import Todo from '../model/Todo';
import TodoItem from './TodoItem';

interface TodoListProps {
  todos?: Todo[];
  selectTodo: ({ id }: { id: number }) => void;
}

const sortTodo = ({ name: aname }: Todo, { name: bname }: Todo) =>
  aname === bname ? 0 : aname > bname ? 1 : -1;

const TodoList: FC<TodoListProps> = ({ todos, selectTodo }: TodoListProps) => {
  if (!todos) return <div>loading...</div>;
  return (
    <ul style={{ paddingLeft: 0 }}>
      {todos.sort(sortTodo).map(todo => (
        <TodoItem key={todo.id} todo={todo} getTodo={selectTodo} />
      ))}
    </ul>
  );
};

export default TodoList;
